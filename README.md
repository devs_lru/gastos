![Alt text](http://www.groupeseb.com/sites/all/themes/sebgroupe/logo.png "Logo GroupEseb")

# Proyecto de Gastos (GroupEseb)
## Wiki: **Contenido de Interés para desarrolladores y usuarios Finales**

## Bienvenido.

Aquí podrá encontrar información respectiva al funcionamiento de la plataforma que esta construida sobre QlikView por la empresa BiPlaning para el análisis de los gastos.

* * *
Procure tener abierta la aplicación final, si desea resolver dudas.

### **Evite modificar código sin realizar consultas con su desarrollador asignado**
Equipo BiPlanning | Correo | Cargo
------------- | ------------- | -------------
Miguel Ángel lemoz  | miguel.lemos@biplanning.com.co | Consultor analítico 
Anderson Londoño  | anderson.londono@biplanning.com.co | Desarrollador BI

* * * 
**Nota:** Esta Wiki es editada continuamente, es recomendable su continúa lectura. 
* * * 

![Alt text](http://www.biplanning.com.co/images/logobiplanning.png "Logo BiPlaning")